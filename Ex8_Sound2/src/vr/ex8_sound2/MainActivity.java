package vr.ex8_sound2;

import java.io.IOException;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity implements Runnable {
	private MediaPlayer mPlayer = null;
	private String mSdPath = Environment.getExternalStorageDirectory()
			.getAbsolutePath();
	private String mMp3File = "test.mp3";
	private ProgressBar mProgress;
	private SeekBar seekbarControl;

	public void showProgress() {
		if (mPlayer != null && mPlayer.isPlaying()) {
			mProgress.setProgress(mPlayer.getCurrentPosition());
			//seekbarControl.setProgress(mPlayer.getCurrentPosition());
			TextView output = (TextView) findViewById(R.id.textView1);
			output.setText("재생중" + "\n현재위치: " + mPlayer.getCurrentPosition()
					/ 1000 + "\n전체길이: " + mPlayer.getDuration() / 1000);
		}
		
	}
	
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		initMP();
		
		seekbarControl = (SeekBar) this.findViewById(R.id.seekBar1);

		seekbarControl
				.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
					@Override
					public void onProgressChanged(SeekBar seekBarControl,
							int progress, boolean fromUser) {
						mPlayer.seekTo(mPlayer.getCurrentPosition() + progress);
						// Toast.makeText( MainActivity.this,
						// "onProgressChanged", Toast.LENGTH_SHORT).show() ;
						// 값이 변경될대 변경된 값이 progress에 들어온다.
					}

					@Override
					public void onStartTrackingTouch(SeekBar seekBar) {
						// Toast.makeText( MainActivity.this,
						// "onStartTrackingTouch", Toast.LENGTH_SHORT).show() ;
						// //SeekBar를 이동하기 위하여 선택하였을때
					}

					@Override
					public void onStopTrackingTouch(SeekBar seekBar) {
						// Toast.makeText( MainActivity.this,
						// "onStopTrackingTouch", Toast.LENGTH_SHORT).show() ;
						// //SeekBar를 이동하다가 멈추었을때
					}

				});

		
	}

	@Override
	protected void onDestroy() {
		if (mPlayer != null) {
			mPlayer.release();
			mPlayer = null;
		}
		super.onDestroy();
	}

	public void initMP() {
		mPlayer = new MediaPlayer();
		mProgress = (ProgressBar) findViewById(R.id.progressBar1);
		seekbarControl = (SeekBar) findViewById(R.id.seekBar1);
		mProgress.setProgress(0);
		mProgress.setMax(0);
		seekbarControl.setProgress(0);
		seekbarControl.setMax(0);
		Thread thrd = new Thread(this);
		thrd.setDaemon(true);
		thrd.start();
	}

	public Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			if (msg.what == 1) {
				showProgress();
			}
		}
	};

	@Override
	public void run() {
		while (mPlayer != null) {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
				break;
			}
			mHandler.sendMessage(Message.obtain(mHandler, 1));
		}
	}

	public void startMP(View v) {
		try {
			mPlayer.setDataSource(mSdPath + "/" + mMp3File);
			mPlayer.prepare();
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
		Button btn;
		btn = (Button) findViewById(R.id.button1);
		btn.setEnabled(false);
		btn = (Button) findViewById(R.id.button2);
		btn.setEnabled(true);
		mProgress.setProgress(0);
		seekbarControl.setProgress(0);
		mProgress.setMax(mPlayer.getDuration());
		seekbarControl.setMax(mPlayer.getDuration());
		mPlayer.setLooping(true);
		mPlayer.start();
	}

	public void pauseMP(View v) {
		if (mPlayer.isPlaying()) {
			mPlayer.pause();
			TextView output = (TextView) findViewById(R.id.textView1);
			output.setText("일시정지");
		} else {
			mPlayer.start();
		}
	}

	public void stopMP(View v) {
		mPlayer.stop();
		mPlayer.reset();
		Button btn;
		btn = (Button) findViewById(R.id.button1);
		btn.setEnabled(true);
		btn = (Button) findViewById(R.id.button2);
		btn.setEnabled(false);
		mProgress.setProgress(0);
		mProgress.setMax(0);
		seekbarControl.setProgress(0);
		seekbarControl.setMax(0);
		TextView output = (TextView) findViewById(R.id.textView1);
		output.setText("중지");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
